#!/bin/bash
#SBATCH --output job.%j.out
#SBATCH --partition=C032M0128G
#SBATCH --job-name 12-6
#SBATCH --nodes=4
#SBATCH --exclusive
#SBATCH --get-user-env
#SBATCH --mail-type=END
#SBATCH --mail-user=hpc_notifications@163.com

srun hostname -s | sort -n > slurm.hosts
mpirun -n 128 -machinefile slurm.hosts pw.x -in nt.in > nt.out
rm -f slurm.hosts
