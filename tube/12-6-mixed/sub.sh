#!/bin/bash
#SBATCH --output job.%j.out
#SBATCH --partition=compute
#SBATCH --job-name 12-6
#SBATCH -N 10
#SBATCH --ntasks-per-node=2
#SBATCH --exclusive
#SBATCH --get-user-env
#SBATCH --mail-type=END
#SBATCH --mail-user=hpc_notifications@163.com

pwd
srun hostname -s | sort -n > slurm.hosts
mpirun -n 20 -machinefile slurm.hosts pw.x -in nt.scf.in > nt.scf.out
rm -f slurm.hosts
