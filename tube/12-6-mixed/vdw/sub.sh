#!/bin/bash
#SBATCH --output job.%j.out
#SBATCH --partition=C032M0256G
#SBATCH --job-name vdw
#SBATCH --nodes=2
#SBATCH --exclusive
#SBATCH --get-user-env
#SBATCH --mail-type=END
#SBATCH --mail-user=hpc_notifications@163.com

pwd
srun hostname -s | sort -n > slurm.hosts
mpirun -n 64 -machinefile slurm.hosts pw.x -in nt.16.in > nt.16.out
mpirun -n 64 -machinefile slurm.hosts pw.x -in nt.16.25.in > nt.16.25.out
mpirun -n 64 -machinefile slurm.hosts pw.x -in nt.16.5.in > nt.16.5.out
mpirun -n 64 -machinefile slurm.hosts pw.x -in nt.17.in > nt.17.out
mpirun -n 64 -machinefile slurm.hosts pw.x -in nt.17.5.in > nt.17.5.out
mpirun -n 64 -machinefile slurm.hosts pw.x -in nt.18.in > nt.18.out
mpirun -n 64 -machinefile slurm.hosts pw.x -in nt.19.in > nt.19.out
mpirun -n 64 -machinefile slurm.hosts pw.x -in nt.20.in > nt.20.out
mpirun -n 64 -machinefile slurm.hosts pw.x -in nt.24.in > nt.24.out
rm -f slurm.hosts
