#!/usr/bin/env python3

import numpy as np
import sys
import os
import subprocess as sp
from scipy.spatial.transform import Rotation as R
import copy
from prompt_toolkit import PromptSession
from prompt_toolkit.history import FileHistory

session = PromptSession('> ', history=FileHistory('.geom_history'))

prec = 10
fmt = '{{: {}.{}f}}'.format(prec + 6, prec)
__vis__ = 1
vis = 0

splinput = lambda: list(filter(lambda e: e, input().split(' ')))
float_ = np.vectorize(float)
int_ = np.vectorize(int)

def printvec(vec): # print row vectors shaped (3,)
    assert len(vec) == 3
    print(' '.join(map(lambda e: fmt.format(round(e, prec) + 0), vec)))

def printcoord(coord):
    print('{:2}'.format(coord[0]), end=' ')
    printvec(coord[1])

def readxyz(_filename):
    stdin = sys.stdin
    sys.stdin = open(_filename, 'r')
    _size = int(input())
    _coord = []
    input()
    while True:
        try:
            s = splinput()
            assert len(s) == 4
            _coord.append([s[0], float_(s[1:])])
        except EOFError:
            break
    assert _size == len(_coord)
    sys.stdin = stdin
    return _coord

coord = readxyz(sys.argv[1])
template = readxyz('single.xyz')
watch = []
rot = R.identity()

while True:
    s = list(filter(lambda e: e, session.prompt().split(' ')))

    if len(s) == 0:
        continue

    elif s[0] == 'sort':
        coord = sorted(coord, key=lambda j: j[1][2])

    elif s[0] == 'watch':
        try:
            if len(s) == 1:
                watch = []
            elif s[1] == 'all':
                watch = list(range(1, len(coord) + 1))
            else:
                watch_ = int_(s[1:])
                if len(watch_) > 0 and not all(i >= 1 and i <= len(coord) for i in watch_):
                    raise ValueError
                watch = watch_
                vis = 1
        except ValueError:
            print('Invalid watch value.')

    elif s[0] == 'trans':
        try:
            if len(s) == 3:
                if s[1] == 'x':
                    transvec = [float(s[2]), 0, 0]
                elif s[1] == 'y':
                    transvec = [0, float(s[2]), 0]
                elif s[1] == 'z':
                    transvec = [0, 0, float(s[2])]
                else:
                    raise ValueError
            elif len(s) == 4:
                transvec = float_(s[1:])
            else:
                raise ValueError
            for j in coord:
                j[1] = np.add(j[1], transvec)
        except ValueError:
            print('Invalid translation parameters.')

    elif s[0] == 'rot':
        try:
            if len(s) == 3:
                deg = float(s[2])
                if s[1] == 'x':
                    r = R.from_rotvec(deg * np.pi / 180 * np.array([1, 0, 0]))
                elif s[1] == 'y':
                    r = R.from_rotvec(deg * np.pi / 180 * np.array([0, 1, 0]))
                elif s[1] == 'z':
                    r = R.from_rotvec(deg * np.pi / 180 * np.array([0, 0, 1]))
                else:
                    raise ValueError
            else:
                raise ValueError
            for j in coord:
                j[1] = r.apply(j[1])
            rot = r * rot
            vis = 1
        except ValueError:
            print('Invalid rotation parameters.')

    elif s[0] == 'rotmat':
        print('Input rotation matrix:')
        try:
            r = float_([splinput(), splinput(), splinput()])
            if r.shape != (3, 3):
                raise ValueError
            r = R.from_matrix(r)
            for j in coord:
                j[1] = r.apply(j[1])
            rot = r * rot
            vis = 1
        except ValueError:
            print('Invalid rotation matrix.')
    
    elif s[0] == 'currot':
        print('Current rotation matrix:')
        for j in rot.as_matrix():
            printvec(j)
    
    elif s[0] == 'vis':
        try:
            if len(s) == 1:
                vis = 1
            elif len(s) == 2:
                if s[1] == '0':
                    __vis__ = 0
                elif s[1] == '1':
                    __vis__ = 1
                else:
                    raise ValueError
            else:
                raise ValueError
        except ValueError:
            print('Invalid visualize mode.')
        

    elif s[0] == 'save':
        filename = s[1]
        if os.path.isfile(filename):
            flag = 0
            while True:
                print('File', filename, 'already exists. Overwrite? (yes/no) ', end='')
                t = ''.join(splinput())
                if t == 'yes':
                    flag = 1
                    break
                elif t == 'no':
                    break
            if flag == 0:
                continue
        elif os.path.isdir(filename):
            print('Directory', filename, 'already exists.')
            continue
        stdout = sys.stdout
        sys.stdout = open(filename, 'w')
        print(len(coord), '\n')
        for j in coord:
            printcoord(j)
        sys.stdout = stdout

    elif s[0] == 'exit':
        sp.Popen(args=['rm', '-f', 'xcrysden.log', 'watch.tmp'])
        break

    else:
        print('Available commands: trans rot save exit watch vis')
        continue

    stdout = sys.stdout
    sys.stdout = open('watch.tmp', 'w')
    print(len(watch) + len(template), '\n')
    for i in watch:
        printcoord(coord[i - 1])
    for j in template:
        printcoord(j)
    sys.stdout = stdout
    for i in watch:
        printcoord(coord[i - 1])
    
    if __vis__ == 1 and vis == 1 and len(watch) != 0:
        sp.Popen(args=['xcrysden', '--xyz', 'watch.tmp'], stdout=sp.DEVNULL, stderr=sp.DEVNULL)
        vis = 0

