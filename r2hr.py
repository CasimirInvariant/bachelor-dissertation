#!/usr/bin/env python3

import numpy as np
import sys
import copy

prec = 9  # target precision
eps = 10 ** -prec  # target epsilon, for assertion
calc_prec = prec + 3  # calculation precision, smaller than 16
bohr = 0.52917720859  # angstrom: Modules/constants.f90
ibrav = 0


def splinput():  # splitted input
    return list(filter(lambda e: e, input().split(' ')))


def latt_add(coord, delta):
    # handles lattice translation for column vectors with shape [[], [], []]
    t = np.add(coord, delta)
    for j in t:
        j[0] = round(j[0], calc_prec)
        j[0] -= np.floor(j[0])
        assert j[0] > 0 - eps and j[0] < 1 - eps
    return t


while True:
    try:
        s = splinput()
        if len(s) > 0:
            if s[0] == 'bravais-lattice':
                ibrav = int(s[-1])
            elif s[0] == 'Begin':  # Begin final coordinates
                break
    except EOFError:
        raise ValueError

input()
input()
input()

float_vec = np.vectorize(float)

# alat, angstrom
alat = float(splinput()[2][:-1]) * bohr
# column vectors, shape(3, 1), angstrom
cell_param = float_vec([splinput(), splinput(), splinput()]).transpose() * alat

input()
input()

coord_cryst = []  # (string, column vector shape(3, 1) in crystal basis)
while True:
    try:
        s = splinput()
        coord_cryst.append((s[0], float_vec(s[1:]).reshape(3, 1)))
    except:
        break

assert ibrav == 5  # r2hr routine

print('ibrav = 5, cell converted from rhombohedral to hR setting', file=sys.stderr)

r2hr_matrix = np.array(
    [[1, -1, 1], [0, 1, 1], [-1, 0, 1]], dtype=float)  # input_pw.html
hr_trans_vec = [[[0], [0], [0]], [
    [1 / 3], [2 / 3], [1 / 3]], [[2 / 3], [1 / 3], [2 / 3]]]
hr_cc = []

for i in coord_cryst:
    x = np.linalg.solve(r2hr_matrix, i[1])
    for t in hr_trans_vec:
        hr_cc.append((i[0], latt_add(x, t)))

coord_cryst = hr_cc
cell_param = cell_param.dot(r2hr_matrix)
cell_A = np.linalg.norm(cell_param.transpose()[0])
cell_B = np.linalg.norm(cell_param.transpose()[1])
cell_C = cell_param[2][2]
cell_param = cell_A * \
    np.array([[1, -1 / 2, 0], [0, np.sqrt(3) / 2, 0], [0, 0, cell_C / cell_A]])
# build (001) slab from command line args: z pos range (start, end), supercell size (? x ? x 1),
# if_mirror, origin (x, y), vacuum thickness

slab_size = 1
slab_range = (0, 1)
slab_mirror = 0
slab_origin = (0, 0)
slab_vacuum = 0  # angstrom
subfile = 0
dosort = 0
crystal = 0
bulk = 1

i = 1
while i < len(sys.argv):
    if sys.argv[i] == 'range':
        i += 2
        slab_range = (float(sys.argv[i - 1]), float(sys.argv[i]))
        assert slab_range[1] - slab_range[0] > eps
    elif sys.argv[i] == 'size':
        i += 1
        slab_size = int(sys.argv[i])
        assert slab_size > 0
    elif sys.argv[i] == 'mirror':
        slab_mirror = 1
    elif sys.argv[i] == 'origin':
        i += 2
        slab_origin = (float(sys.argv[i - 1]), float(sys.argv[i]))
        assert slab_origin[0] > 0 - eps and slab_origin[0] < 1 - eps
        assert slab_origin[1] > 0 - eps or slab_origin[1] < 1 - eps
    elif sys.argv[i] == 'vacuum':
        i += 1
        slab_vacuum = float(sys.argv[i])
        if slab_vacuum == 0:
            bulk = 1
            print('bulk mode, origin shift only', file=sys.stderr)
        else:
            bulk = 0
            assert slab_vacuum >= 10
    elif sys.argv[i] == 'subfile':
        subfile = 1
    elif sys.argv[i] == 'sort':
        dosort = 1
    elif sys.argv[i] == 'crystal':
        crystal = 1
    else:
        print(
            'Available commands: range size mirror origin vacuum subfile sort crystal', file=sys.stderr)
        exit()
    i += 1

slab_coord_cryst = [] # in hR crystal axes, fractional

for j in coord_cryst:

    t = copy.deepcopy(j[1]).reshape(3) # (3, 1) -> (3,)
    while t[2] > slab_range[0] - eps:
        t[2] -= 1
    t[2] += 1
    while t[2] > slab_range[0] - eps and t[2] < slab_range[1] - eps:
        for l in range(slab_size):
            for k in range(slab_size):
                slab_coord_cryst.append((j[0], np.array([
                    t[0] + l - slab_origin[0],
                    t[1] + k - slab_origin[1],
                    t[2]])
                    .reshape(3, 1)))
        t[2] += 1

slab_max_z = max(j[1][2][0] for j in slab_coord_cryst)
slab_min_z = min(j[1][2][0] for j in slab_coord_cryst)
if bulk == 1:
    assert int(slab_range[0] - slab_range[1]) == slab_range[0] - slab_range[1]
    slab_C = (slab_range[1] - slab_range[0]) * cell_C
else:
    slab_C = (slab_max_z - slab_min_z) * cell_C + slab_vacuum

for j in slab_coord_cryst:
    if slab_mirror == 0:
        j[1][2][0] -= slab_min_z
    else:
        j[1][2][0] = -j[1][2][0] + slab_max_z

if dosort == 1:
    slab_coord_cryst = sorted(slab_coord_cryst, key=lambda j: j[1][2][0])

if subfile == 1:
    print('#!/bin/bash')
    print('R2HR_FILENAME=$(basename $0 .sh).in')
    print('sed "1i\\! Generated with command r2hr.py',
          ' '.join(sys.argv[1:]), '" | \\')
else:
    print('! Generated with command r2hr.py', ' '.join(sys.argv[1:]))
fmt = '{{:.{}f}}'.format(prec)

if subfile == 1:
    print('sed "s/A = .*/A =', fmt.format(cell_A * slab_size), '/g" | \\')
    print('sed "s/C = .*/C =', fmt.format(slab_C), '/g" | \\')
    print('sed "s/nat = .*/nat =', len(slab_coord_cryst), '/g" > $R2HR_FILENAME')
else:
    print('A =', fmt.format(cell_A * slab_size))
    print('C =', fmt.format(slab_C))
    print('nat = {}'.format(len(slab_coord_cryst)))
print()
if subfile == 1:
    print('cat >> $R2HR_FILENAME << EOF')
print()
if crystal == 0:
    print('ATOMIC_POSITIONS angstrom')
    fmt = '{{: {}.{}f}}'.format(prec + 7, prec)
    for j in slab_coord_cryst:
        print('{:2}'.format(j[0]), ' '.join(map(lambda e: fmt.format(
            round(e, prec) + 0), cell_param.dot(j[1]).reshape(3))))
else:
    print('ATOMIC_POSITIONS crystal')
    fmt = '{{: {}.{}f}}'.format(prec + 7, prec)
    for j in slab_coord_cryst:
        t = j[1].reshape(3)
        print('{:2}'.format(j[0]), ' '.join(map(lambda e: fmt.format(
            round(e, prec) + 0), [t[0] / slab_size, t[1] / slab_size, t[2] * cell_C / slab_C])))
if subfile == 1:
    print('EOF')
print()
sys.stdout.flush()
