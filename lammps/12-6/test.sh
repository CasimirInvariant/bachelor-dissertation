#!/bin/bash

echo > lmp.log

for ((i=0;i<=4;i++)); do
    for ((j=0;j<=i;j++)); do
        for ((t=0;t<=4;t++)); do
            sed '/variable       loopx equal/s/$/ '$i'/' in.full > in.tmp.$i-$j-$t
            sed -i '/variable       loopy equal/s/$/ '$j'/' in.tmp.$i-$j-$t
            sed -i '/variable       looptheta equal/s/$/ '$t'/' in.tmp.$i-$j-$t
            echo testing in.tmp.$i-$j-$t
            mpirun -np 8 lmp_mpi -in in.tmp.$i-$j-$t >> lmp.log
            rm in.tmp.$i-$j-$t
        done
    done 
done