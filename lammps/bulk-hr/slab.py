#!/usr/bin/env python3

import numpy as np

prec = 10
fmt = '{{: {}.{}f}}'.format(prec + 6, prec)


splinput = lambda: list(filter(lambda e: e, input().split(' ')))
float_ = np.vectorize(float)
int_ = np.vectorize(int)

def printarray(arr): 
    print(' '.join(map(lambda e: fmt.format(round(e, prec) + 0), arr)), end=' ')

xhi = 0
zhi = 0
yhi = 0
xy = 0
nat = 0
ntyp = 0
mass = []
while True:
    s = splinput()
    if len(s) >= 3 and s[2] == 'xlo':
        xhi = float(s[1])
        s = splinput()
        s = splinput()
        zhi = float(s[1])
        yhi = xhi * np.sqrt(3) / 2
        xy = -1 / 2 * xhi
    elif len(s) >= 2 and s[1] == 'atoms':
        nat = int(s[0])
        s = splinput()
        ntyp = int(s[0])
    elif len(s) >= 1 and s[0] == 'Masses':
        s = splinput()
        for i in range(ntyp):
            mass.append(input())
    elif len(s) >= 1 and s[0] == 'Atoms':
        break

latt_param = np.array([[xhi, 0, 0], [xy, yhi, 0], [0, 0, zhi]]).transpose()
coord = []
input()
for i in range(nat):
    s = splinput()
    c = float_(s[2:5]) + np.dot(latt_param, int_(s[5:8]).transpose()).transpose()
    coord.append([int(s[0]), int(s[1]), c])

coord = filter(lambda x: x[2][2] < zhi * 0.93 and x[2][2] > zhi * 0.63, coord)
coord = sorted(coord, key=lambda x: x[2][2])

minz = coord[0][2][2]
size = 8
print('LAMMPS data file via slab.py'); print()
print(len(coord) * size * size , 'atoms')
print(ntyp, 'atom types')
print()
print(0., xhi * size, 'xlo xhi')
print(0., yhi * size, 'ylo yhi')
print(0., 40., 'zlo zhi') # vacuum
print(xy * size, 0., 0., 'xy xz yz')
print()
print('Masses'); print()
for i in range(ntyp):
    print(mass[i])
print()
print('Atoms # atomic'); print()
count = 1
for j in coord:
    j[2][2] -= minz
    for l in range(size):
        for m in range(size):
            print('{:4}'.format(count), '{:4}'.format(j[1]), end=' ')
            printarray([j[2][0] + l * xhi + m * xy, j[2][1] + m * yhi, j[2][2]])
            print()
            count += 1