#!/usr/bin/env python3

# check vacuum layer length == 10 angstrom

def splinput():  # splitted input
    return list(filter(lambda e: e, input().split(' ')))

C = 0
while True:
    s = splinput()
    if len(s) == 0:
        continue
    if s[0] == 'C':
        C = float(s[2])
    if s[0] == 'ATOMIC_POSITIONS':
        break

maxZ = -1
while True:
    try:
        s = splinput()
        if maxZ < float(s[-1]):
            maxZ = float(s[-1])
    except EOFError:
        break

assert abs(C * (1 - maxZ) - 10) < 1e-6