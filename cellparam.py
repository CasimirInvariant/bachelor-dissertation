#!/usr/bin/env python3
from prompt_toolkit import PromptSession
from numpy import sqrt

session = PromptSession()

prec = 10
fmt = '{{: {}.{}f}}'.format(prec + 7, prec)
A = float(session.prompt('A = '))
C = float(session.prompt('C = '))
print(''.join(map(fmt.format, [A, 0, 0])),
      ''.join(map(fmt.format, [-A/2, sqrt(3)*A/2, 0])),
      ''.join(map(fmt.format, [0, 0, C])),
      sep='\n')
