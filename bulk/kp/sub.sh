#!/bin/bash
#SBATCH --output job.%j.out
#SBATCH --partition=compute
#SBATCH --job-name CoW
#SBATCH -N 10
#SBATCH --ntasks-per-node=2
#SBATCH --exclusive
#SBATCH --get-user-env
#SBATCH --mail-type=END
#SBATCH --mail-user=hpc_notifications@163.com

pwd
srun hostname -s | sort -n > slurm.hosts
mpirun -n 20 -machinefile slurm.hosts pw.x -nk 4 -in bulk.scf.11.in > bulk.scf.11.out
./clean
mpirun -n 20 -machinefile slurm.hosts pw.x -nk 4 -in bulk.scf.12.in > bulk.scf.12.out
./clean
rm -f slurm.hosts
