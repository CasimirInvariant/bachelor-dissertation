#!/bin/bash
#SBATCH --output job.%j.out
#SBATCH --partition=compute
#SBATCH --job-name CoW
#SBATCH -N 10
#SBATCH --ntasks-per-node=2
#SBATCH --exclusive
#SBATCH --get-user-env
#SBATCH --mail-type=END
#SBATCH --mail-user=hpc_notifications@163.com

pwd
srun hostname -s | sort -n > slurm.hosts
mpirun -n 20 -machinefile slurm.hosts pw.x -nk 4 -in bulk.scf.100.in > bulk.scf.100.out
./clean
mpirun -n 20 -machinefile slurm.hosts pw.x -nk 4 -in bulk.scf.110.in > bulk.scf.110.out
./clean
mpirun -n 20 -machinefile slurm.hosts pw.x -nk 4 -in bulk.scf.120.in > bulk.scf.120.out
./clean
mpirun -n 20 -machinefile slurm.hosts pw.x -nk 4 -in bulk.scf.130.in > bulk.scf.130.out
./clean
mpirun -n 20 -machinefile slurm.hosts pw.x -nk 4 -in bulk.scf.140.in > bulk.scf.140.out
./clean
rm -f slurm.hosts
