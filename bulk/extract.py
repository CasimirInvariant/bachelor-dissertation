#!/usr/bin/env python3

import numpy as np
import sys
import copy

prec = 9  # target precision
eps = 10 ** -prec # target epsilon, for assertion
calc_prec = prec + 3  # calculation precision, smaller than 16
bohr = 0.52917720859

def splinput():  # splitted input
    return list(filter(lambda e: e, input().split(' ')))

while True:
    try:
        s = splinput()
        if len(s) > 0 and s[0] == 'Begin':  # Begin final coordinates
                break
    except EOFError:
        raise ValueError

input()
input()
input()

float_vec = np.vectorize(float)

# alat, bohr
alat = float(splinput()[2][:-1])
# row vectors, shape(3,), alat
cp1 = float_vec(splinput())
cp2 = float_vec(splinput())
cp3 = float_vec(splinput())


print('A =', alat * np.linalg.norm(cp1) * bohr)
print('gamma =', np.arccos(np.dot(cp1, cp2) / np.dot(cp1, cp1)) / np.pi * 180)