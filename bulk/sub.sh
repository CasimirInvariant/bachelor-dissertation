#!/bin/bash
#SBATCH --output job.%j.out
#SBATCH --partition=C032M0128G
#SBATCH --job-name CoW
#SBATCH --nodes=2
#SBATCH --exclusive
#SBATCH --get-user-env
#SBATCH --qos=normal
#SBATCH --mail-type=END
#SBATCH --mail-user=hpc_notifications@163.com

FILENAME=bulk.col
srun hostname -s | sort -n > $FILENAME.hosts
mpirun -n 64 -machinefile $FILENAME.hosts pw.x -nk 4 -in $FILENAME.in > $FILENAME.out
rm -f $FILENAME.hosts
